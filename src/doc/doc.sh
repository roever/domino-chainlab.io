#!/bin/sh
set -eu

target=$1

doc() {
  id='domino-chain'
  binary='domino-chain'
  name='Domino-Chain'
  summary='Rearrange dominoes on different platforms to start a chain reaction.'
  license='GPL-3.0+'
  website='https://domino-chain.gitlab.io/'
  issue_tracker='https://gitlab.com/domino-chain/domino-chain.gitlab.io/issues'
  doc_head
  doc_description
  doc_description_p
  doc_description_p_line 'Domino-Chain is a puzzle game where you have to rearrange'
  doc_description_p_line 'dominoes on different platforms to start a chain reaction that'
  doc_description_p_line 'makes all dominoes topple over. There are many strange types'
  doc_description_p_line 'of dominoes, such as the Ascender which will rise to the'
  doc_description_p_line 'ceiling when pushed, or the Exploder which will blast a hole'
  doc_description_p_line 'into the platform it stands on.'
  doc_description_p_end
  doc_description_p
  doc_description_p_link 'Domino-Chain is a faithful reincarnation of the game ' 'Pushover' '' 'https://en.wikipedia.org/wiki/Pushover_(video_game)'
  doc_description_p_line 'originally published by Ocean in 1992. Compared to Pushover,'
  doc_description_p_line 'Domino-Chain has some new levels, some additional domino'
  doc_description_p_line 'types, better graphics in higher resolution and high-quality'
  doc_description_p_line 'music. On top of that, you can load and play the original'
  doc_description_p_line 'levels from Pushover if you have a copy of it.'
  doc_description_p_end
  doc_description_p
  doc_description_p_link 'This game is ' 'free software' ' and created by volunteers. Even' 'https://en.wikipedia.org/wiki/Free_software'
  doc_description_p_line 'though it is in a pretty good state, there is a lot to'
  doc_description_p_line 'improve. If you like Domino-Chain, please consider to join the'
  doc_description_p_line 'team and to help with translations, levels, graphics and more.'
  doc_description_p_end
  doc_description_end
  doc_nav
  doc_nav_line 'Install' 'install'
  doc_nav_line 'Play' 'play'
  doc_nav_line 'Contribute' 'contribute'
  doc_nav_end
  doc_noman doc_section 'Install' 'install'
  doc_noman doc_subsection 'Debian, Mint, Ubuntu' 'debian'
  doc_noman doc_p
  doc_noman doc_p_line 'On Debian, Mint, Ubuntu and similar systems, Domino-Chain'
  doc_noman doc_p_line 'should already be available. Just install the package'
  doc_noman doc_p_line '"domino-chain".'
  doc_noman doc_p_end
  doc_noman doc_p
  doc_noman doc_p_link 'Otherwise, please ' 'build from source.' '' '#source'
  doc_noman doc_p_end
  doc_noman doc_subsection 'Windows' 'windows'
  doc_noman doc_p
  doc_noman doc_p_line 'Since Windows lacks a proper package management system, you'
  doc_noman doc_p_line 'need to download and unpack the package by hand:'
  doc_noman doc_p_end
  doc_noman doc_linkline 'Domino-Chain for Windows' "https://gitlab.com/domino-chain/domino-chain.gitlab.io$(cat src/doc/upload-url-dist-windows)"
  doc_noman doc_subsection 'Mac OS X' 'macosx'
  doc_noman doc_p
  doc_noman doc_p_line 'This system is not yet supported, but it should be possible'
  doc_noman doc_p_link 'without too much hassle. Please ' 'get in contact with us' ' if' '#contribute'
  doc_noman doc_p_line "you'd like to volunteer."
  doc_noman doc_p_end
  doc_noman doc_subsection 'Source' 'source'
  doc_noman doc_p
  doc_noman doc_p_line 'If your operating system is not listed above, or you want to'
  doc_noman doc_p_line 'try the latest development version, you can build from source.'
  doc_noman doc_p_line 'You need:'
  doc_noman doc_p_end
  doc_noman doc_dl
  doc_noman doc_dl_item 'Tools:'
  doc_noman doc_dl_item_line 'GCC/g++, gettext, Git, ImageMagick, Make, pkgconf,'
  doc_noman doc_dl_item_line 'POV-Ray, sed, shellcheck'
  doc_noman doc_dl_item_end
  doc_noman doc_dl_item 'Libraries:'
  doc_noman doc_dl_item_line 'Boost filesystem, Boost system, FriBidi, LUA, libpng,'
  doc_noman doc_dl_item_line 'SDL2, SDL2_image, SDL2_mixer, SDL2_ttf, zlib'
  doc_noman doc_dl_item_end
  doc_noman doc_dl_item 'Fonts:'
  doc_noman doc_dl_item_line 'FreeSans'
  doc_noman doc_dl_item_end
  doc_noman doc_dl_end
  doc_noman doc_p
  doc_noman doc_p_line 'On Debian-based systems, this means:'
  doc_noman doc_p_end
  doc_noman doc_pre
  packages='
    fonts-freefont-ttf
    g++
    gettext
    imagemagick
    libboost-filesystem-dev
    libboost-system-dev
    libfribidi-dev
    liblua5.2-dev
    libpng-dev
    libsdl2-dev
    libsdl2-image-dev
    libsdl2-mixer-dev
    libsdl2-ttf-dev
    make
    pkgconf
    povray
    shellcheck
    zlib1g-dev
  '
  doc_noman doc_pre_line "sudo apt-get install $(echo "$packages" | xargs)"
  doc_noman doc_pre_end
  doc_noman doc_p
  doc_noman doc_p_line 'Clone the Git repository, build the game and run it as follows:'
  doc_noman doc_p_end
  doc_noman doc_pre
  doc_noman doc_pre_line 'git clone https://gitlab.com/domino-chain/domino-chain.gitlab.io.git domino-chain'
  doc_noman doc_pre_line 'cd domino-chain'
  doc_noman doc_pre_line 'make'
  doc_noman doc_pre_line './domino-chain'
  doc_noman doc_pre_end
  doc_noman doc_p
  doc_noman doc_p_line 'Have fun!'
  doc_noman doc_p_end
  doc_man_section 'USAGE'
  doc_section 'Play' 'play'
  doc_subsection 'Rules' 'rules'
  doc_p
  doc_p_line 'You control a figure that can walk along platforms which are'
  doc_p_line 'connected with ladders. On those platforms there are dominoes'
  doc_p_line 'that will fall according to certain rules. Your task is to'
  doc_p_line 'rearrange the dominoes so you can start a chain reaction that'
  doc_p_line 'makes all dominoes topple over. The rules are:'
  doc_p_end
  doc_dl
  doc_dl_item 'Domino types:'
  doc_dl_item_line 'There are many different dominoes that behave differently'
  doc_dl_item_line 'when pushed. Some fall, some not, some wait a bit before'
  doc_dl_item_line 'they fall, some raise, some topple until they meet an'
  doc_dl_item_line 'obstacle.'
  doc_dl_item_end
  doc_dl_item 'All fall, no crash:'
  doc_dl_item_line 'All dominoes (except for the blocker) must fall and none'
  doc_dl_item_line 'must crash into another. They are allowed to fall off the'
  doc_dl_item_line 'screen, though.'
  doc_dl_item_end
  doc_dl_item 'Trigger and exit door:'
  doc_dl_item_line 'The trigger domino must fall as last domino. Only then it'
  doc_dl_item_line 'triggers the exit door to open. When you enter the exit door'
  doc_dl_item_line 'the level has been completed.'
  doc_dl_item_end
  doc_dl_item 'Disallowed moves:'
  doc_dl_item_line 'You may rearrange as many dominoes as you want, except for'
  doc_dl_item_line 'the trigger. You must not place dominoes in front of the'
  doc_dl_item_line 'doors, except for the vanishing domino.'
  doc_dl_item_end
  doc_dl_item 'One push:'
  doc_dl_item_line 'You may push only once to start a chain reaction with the'
  doc_dl_item_line 'dominoes leading to the fall of all of them.'
  doc_dl_item_end
  doc_dl_item 'Time limit:'
  doc_dl_item_line 'All this has to be done within a time limit. This limit is'
  doc_dl_item_line 'usually generous, but not always.'
  doc_dl_item_end
  doc_dl_end
  doc_p
  doc_p_line 'There is an in-game help as well as and introductory levels'
  doc_p_line 'that show how all the dominoes work.'
  doc_p_end
  doc_subsection 'Dominoes' 'dominoes'
  doc_p
  doc_p_line 'The following types of dominoes exist:'
  doc_p_end
  doc_dl
  doc_dl_item 'Standard, completely yellow:'
  doc_dl_item_line 'There is nothing special with this stone, it behaves like'
  doc_dl_item_line 'a regular domino and falls when pushed.'
  doc_dl_item_end
  doc_dl_item 'Blocker, completely red:'
  doc_dl_item_line "This domino can't fall over, so it is the only kind"
  doc_dl_item_line 'of stone that may still be standing when the level is'
  doc_dl_item_line 'solved. Dominoes falling against this stone will bounce'
  doc_dl_item_line 'back, if possible.'
  doc_dl_item_end
  doc_dl_item 'Tumbler, big red stripe:'
  doc_dl_item_line 'This domino will stand up again after falling and will'
  doc_dl_item_line 'continue to tumble until it hits an obstacle or rests'
  doc_dl_item_line 'against another stone.'
  doc_dl_item_end
  doc_dl_item 'Delay stone, diagonally divided:'
  doc_dl_item_line 'This domino will take some time until it falls, when'
  doc_dl_item_line 'it is pushed. Dominoes falling against this stone will'
  doc_dl_item_line 'bounce back and later this stone will fall.'
  doc_dl_item_end
  doc_dl_item 'Splitter, horizontally divided:'
  doc_dl_item_line 'This stone will split into two stones, one falling to'
  doc_dl_item_line 'the left and the other falling to the right. The splitter'
  doc_dl_item_line "can't be pushed. It must be split by a stone falling onto"
  doc_dl_item_line 'it from above. A pile of rubbish falling into it also'
  doc_dl_item_line 'activates this domino.'
  doc_dl_item_end
  doc_dl_item 'Exploder, vertically divided:'
  doc_dl_item_line 'This stone will blast a gap into the platform it is'
  doc_dl_item_line 'standing on, when it is pushed.  Neither the figure nor'
  doc_dl_item_line 'the pushing domino are harmed by that, the pushing domino'
  doc_dl_item_line 'will fall into the gap.'
  doc_dl_item_end
  doc_dl_item 'Bridger, 1 horizontal strip:'
  doc_dl_item_line 'The bridger will try to connect the edge it is standing'
  doc_dl_item_line 'on with the next edge, if it is close enough, if not it'
  doc_dl_item_line 'will simply fall into the gap.'
  doc_dl_item_end
  doc_dl_item 'Vanisher, 2 horizontal strips:'
  doc_dl_item_line 'The Vanisher will disappear as soon as it lies flat on'
  doc_dl_item_line 'the ground. This is the only stone you may place in front'
  doc_dl_item_line 'of doors.'
  doc_dl_item_end
  doc_dl_item 'Trigger, 3 horizontal strips:'
  doc_dl_item_line 'This stone will open the exit door, as soon as it lies'
  doc_dl_item_line 'completely flat and all other conditions are met (see'
  doc_dl_item_line 'above). This is the only stone that you are not allowed'
  doc_dl_item_line 'to move around.'
  doc_dl_item_end
  doc_dl_item 'Ascender, vertical strip:'
  doc_dl_item_line 'This stone will start to rise as soon as it is pushed. It'
  doc_dl_item_line 'will rise until is hits the ceiling, then it will start to'
  doc_dl_item_line 'flip into the direction it was initially pushed. When you'
  doc_dl_item_line 'fall into a gap while holding this stone it will also rise'
  doc_dl_item_line 'and stay at the ceiling until pushed.'
  doc_dl_item_end
  doc_dl_item 'Quantum, diagonally divided:'
  doc_dl_item_line 'All dominoes of this type will fall together as if they'
  doc_dl_item_line 'were quantum entangled. There are two types of quantum'
  doc_dl_item_line 'dominoes with inverted color markings. Those of the same'
  doc_dl_item_line 'type will fall into the same direction, the others will'
  doc_dl_item_line 'fall into the opposite direction. This is a new type of'
  doc_dl_item_line 'dominoes not present in the original Pushover game.'
  doc_dl_item_end
  doc_dl_end
  doc_subsection 'Controls' 'controls'
  doc_p
  doc_p_line 'The figure is controlled using the cursor keys and space. Use'
  doc_p_line 'the space key to pick up the domino behind the figure or to'
  doc_p_line 'place it down where you are currently standing. To push press'
  doc_p_line 'first up to let the figure enter the row of dominoes. Then'
  doc_p_line 'simultaneously press space and either left or right cursor key'
  doc_p_line 'depending on whether you want to push the domino to your left'
  doc_p_line 'or your right.'
  doc_p_end
  doc_subsection 'Hints' 'hints'
  doc_p
  doc_p_line "If you don't know where to start in a level, simply push a"
  doc_p_line 'stone and observe what happens. This helps very often to get'
  doc_p_line 'a general idea how to solve a level and where the problem is.'
  doc_p_end
  doc_p
  doc_p_line 'If you forgot which domino has what kind of special property'
  doc_p_line 'press F1 to get a short help. This window also displays a'
  doc_p_line 'short hint, once the time of the level is out.'
  doc_p_end
  doc_p
  doc_p_line 'The first levels introduce you to the dominoes. Here you'
  doc_p_line 'can explore how the different dominoes behave in different'
  doc_p_line 'situations.'
  doc_p_end
  doc_section 'Contribute' 'contribute'
  doc_man_section 'CONTACT'
  doc_noman doc_subsection 'Contact' 'contact'
  doc_p
  doc_p_line 'You can file bug reports, discuss ideas for new features and'
  doc_p_line 'offer contributions on the'
  doc_p_end
  doc_linkline 'Domino-Chain Issue Tracker' "$issue_tracker"
  doc_p
  doc_p_line 'which is the central point to get in contact with the project.'
  doc_p_line "There, you'll also find links to the code repository, and so"
  doc_p_line 'on. If you need to contact us privately, please contact'
  doc_p_link 'Andreas Röver via ' 'roever at users dot sourceforge dot net' '.' 'mailto:roever_at_users_dot_sourceforge_dot_net?subject=Domino-Chain'
  doc_p_end
  doc_man_section 'AUTHORS'
  doc_noman doc_subsection 'Authors' 'authors'
  doc_dl
  doc_author 'Andreas Röver'        ' ' 'roever@users.sf.net'     'Project founder, main programming and reverse engineering.'
  doc_author 'Volker Diels-Grabsch' ''  'v@njh.eu'                'Text-based level format, build system and code assorted bugfixes and patches.'
  doc_author 'Roberto Lorenz'       ''  ''                        'Music composition and arrangement.'
  doc_author 'Harald Radke'         ''  'harryrat@postnuklear.de' 'Theme graphics.'
  doc_dl_end
  doc_man_section 'FILES'
  doc_noman doc_subsection 'Files' 'files'
  doc_p
  doc_p_line 'Domino-Chain places a few files on your hard-disc in everyday'
  doc_p_line 'running. Those files will be placed in your home directory.'
  doc_noman doc_p_line 'The exact location depends on your operating system.'
  doc_p_end
  doc_dl
  doc_dl_item 'Location on Unix systems:'
  doc_dl_item_line '~''/.local/share/domino-chain'
  doc_dl_item_end
  doc_noman doc_dl_item 'Location on Windows:'
  doc_noman doc_dl_item_line 'My Documents\domino-chain'
  doc_noman doc_dl_item_end
  doc_dl_end
  doc_p
  doc_p_line 'The following files are saved:'
  doc_p_end
  doc_dl
  doc_dl_item 'solved.txt:'
  doc_dl_item_line 'This file contains checksums of all the levels that you have'
  doc_dl_item_line 'successfully solved. In the level selection dialogue those'
  doc_dl_item_line 'levels contain a mark. If you loose this file those marks'
  doc_dl_item_line 'are gone.'
  doc_dl_item_end
  doc_dl_item '*.rec:'
  doc_dl_item_line 'These files contain recordings of activities within a level.'
  doc_dl_item_line 'They are automatically created whenever you solve a level'
  doc_dl_item_line "but you can also actively make a recording by pressing 'r'"
  doc_dl_item_line 'while you play a level. When you observe something strange'
  doc_dl_item_line 'while playing, make a recording and send it to us. Also when'
  doc_dl_item_line 'the game crashes a recording will be saved. You can delete'
  doc_dl_item_line 'these files whenever you want. You can distinguish the'
  doc_dl_item_line 'recordings by the prefix in their name. "Sol" stands for'
  doc_dl_item_line 'solved levels, "Man" for manually created recordings and'
  doc_dl_item_line '"Err" for recordings made when the program crashed.'
  doc_dl_item_end
  doc_dl_end
  doc_man_section 'NOTES'
  doc_subsection 'Level Designers' 'levels'
  doc_p
  doc_p_line 'Domino-Chain will eventually get a level editor, but right now'
  doc_p_line "it doesn't. However, all levels are stored in a readable plain"
  doc_p_line 'text format, so you can create and modify levels directly with'
  doc_p_line 'a text editor.'
  doc_p_end
  doc_p
  doc_p_line 'To get your levels officially included in the Domino-Chain'
  doc_p_line 'project, you need to adhere to the following rules:'
  doc_p_end
  doc_dl
  doc_dl_item 'License:'
  doc_dl_item_line 'They need to be put under the GPL, or a compatible license.'
  doc_dl_item_line 'Otherwise inclusion is legally not possible. Copyright stays'
  doc_dl_item_line 'with you, of course.'
  doc_dl_item_end
  doc_dl_item 'Complete sets:'
  doc_dl_item_line 'Please contribute only complete sets and no single levels.'
  doc_dl_item_line "They don't need to be long, 10 levels is enough, but this"
  doc_dl_item_line 'way you can keep up a constant scheme and theme logic of the'
  doc_dl_item_line 'levels.'
  doc_dl_item_end
  doc_dl_item 'Solutions:'
  doc_dl_item_line 'You absolutely must provide a recording of one possible'
  doc_dl_item_line 'solution to each level. That solution is not within the'
  doc_dl_item_line 'distribution, just within the source code repository. It is'
  doc_dl_item_line 'used to ensure that the solution of your level is still'
  doc_dl_item_line 'possible after we made changes to the program. This way we'
  doc_dl_item_line 'limit possible frustration.'
  doc_dl_item_end
  doc_dl_item 'Non-compressed:'
  doc_dl_item_line 'Do only send non-compressed level sets. We need those for'
  doc_dl_item_line 'the inclusion in the source. And we need those for possible'
  doc_dl_item_line 'future updates.'
  doc_dl_item_end
  doc_dl_item 'Index:'
  doc_dl_item_line "Use the index file to reorder levels, don't rename the"
  doc_dl_item_line 'files. This way inserting a level becomes very easy.'
  doc_dl_item_end
  doc_dl_end
  doc_subsection 'Graphics' 'graphics'
  doc_p
  doc_p_line 'We have graphics for most themes, but not for all. Those'
  doc_p_line 'themes do have some foreground graphics, but their background'
  doc_p_line 'tiles are all black. You are very much invited to create and'
  doc_p_line 'to improve the graphics. Even more important, though, is the'
  doc_p_line 'creation of a new main figure. In either case, please contact'
  doc_p_line 'us if you are interested.'
  doc_p_end
  doc_p
  doc_p_line 'The backgrounds are made out of 20x13 tiles. Each tile has a'
  doc_p_line 'size of 40x48 pixel. The reason for that is the non square'
  doc_p_line 'pixel of the 320x200 resolution of the original game. For each'
  doc_p_line 'theme there is a PNG image file containing all the blocks that'
  doc_p_line 'may be used by the levels. To make it possible to place the'
  doc_p_line 'blocks more freely into the PNG file a LUA file accompanies'
  doc_p_line 'the image. This LUA file contains the block positions of all'
  doc_p_line 'the used blocks.'
  doc_p_end
  doc_p
  doc_p_line 'It is already implemented to use transparency within the'
  doc_p_line 'blocks. Right now  you can stack up to 8 layers, but if'
  doc_p_line 'necessary this can be made dynamic.'
  doc_p_end
  doc_p
  doc_p_line 'It is also planned to have something like animated tiles, but'
  doc_p_line 'they have to be kept at a low count. Not too many frames and'
  doc_p_line 'not too many animations. They are not intended to make the'
  doc_p_line 'background dynamic, but to rather be a little finishing touch'
  doc_p_line 'to the graphics. Possibilities are trees that move from time'
  doc_p_line 'to time in a breeze, a bird that sails through the sky from'
  doc_p_line 'time to time, and so on.'
  doc_p_end
  doc_p
  doc_p_line 'The figure is more complicated. The image figure.png contains'
  doc_p_line 'all possible animation images for the figure, one animation'
  doc_p_line 'below the other. On request we can provide an additional GIMP'
  doc_p_line 'image that contains in separate layers possible surroundings'
  doc_p_line 'of the figure in different animation frames (like ladders,'
  doc_p_line 'steps, ground, a carried domino, and so on). We will happily'
  doc_p_line 'provide that image to an interested artist.'
  doc_p_end
  doc_noman doc_section 'Changelog' 'changelog'
  doc_noman doc_release '1.1'
  doc_noman doc_dl
  doc_noman doc_dl_item 'Improved gameplay:'
  doc_noman doc_dl_item_line 'The automatic set down of dominoes is disabled, as this'
  doc_noman doc_dl_item_line 'mechanism is irritating for novice and young players.'
  doc_noman doc_dl_item_end
  doc_noman doc_dl
  doc_noman doc_dl_item 'Port to SDL 2:'
  doc_noman doc_dl_item_link 'The whole project is ported from SDL 1 to ' 'SDL 2' ', making the' 'https://www.libsdl.org/'
  doc_noman doc_dl_item_line 'code base more future-proof.'
  doc_noman doc_dl_item_end
  doc_noman doc_dl_item 'Improved build system:'
  doc_noman doc_dl_item_line 'The build system now provides a fully automated cross build'
  doc_noman doc_dl_item_link 'for Windows using ' 'MXE' '. Along the way, several issues regarding' 'https://mxe.cc/'
  doc_noman doc_dl_item_line 'cross builds are fixed.'
  doc_noman doc_dl_item_end
  doc_noman doc_dl_item 'Typos and translations:'
  doc_noman doc_dl_item_line 'Multiple spelling mistakes are fixed and translations are'
  doc_noman doc_dl_item_line 'updated.'
  doc_noman doc_dl_item_end
  doc_noman doc_dl_end
  doc_noman doc_release '1.0'
  doc_noman doc_dl
  doc_noman doc_dl_item 'Initial release:'
  doc_noman doc_dl_item_line 'This is the initial version of the Domino-Chain project.'
  doc_noman doc_dl_item_end
  doc_noman doc_dl_end
  doc_man_section 'CREDITS'
  doc_section 'Credits' 'credits'
  doc_p
  doc_p_line 'We want to thank the developers of the original game for'
  doc_p_line 'making such a nice little game.'
  doc_p_end
  doc_foot
}

doc_noman() {
  if [ "${target}" != man ]; then
    "$@"
  fi
}

doc_line() {
  if [ "$1" = "${target}" ]; then
    echo "$2"
  fi
}

doc_head() {
  doc_line desktop '[Desktop Entry]'
  doc_line desktop 'Type=Application'
  doc_line desktop 'Terminal=false'
  doc_line desktop "Name=${name}"
  doc_line desktop "Comment=${summary}"
  doc_line desktop 'Categories=Game;LogicGame;'
  doc_line desktop "Exec=${binary}"
  doc_line desktop "Icon=${id}"
  doc_line html '<!doctype html>'
  doc_line html '<html lang="en">'
  doc_line html '<head>'
  doc_line html '<meta http-equiv="Content-Type" content="text/html;charset=utf-8">'
  doc_line html '<link rel="stylesheet" href="assets/style.css">'
  doc_line html "<title>${name}</title>"
  doc_line html '</head>'
  doc_line html '<body>'
  doc_line html '<header>'
  doc_line html "<h1>${name}</h1>"
  doc_line html '</header>'
  doc_line html '<div class="section">'
  doc_line html '<div class="summary">'"${summary}</div>"
  doc_line man ".TH $(echo "${name}" | tr '[:lower:]' '[:upper:]') 6"
  doc_line man '.SH NAME'
  doc_line man "${name}"' \- '"${summary}"
  doc_line man '.SH SYNOPSIS'
  doc_line man ".B ${binary}"
  doc_line metainfo '<?xml version="1.0" encoding="UTF-8"?>'
  doc_line metainfo '<component type="desktop-application">'
  doc_line metainfo "<id>${id}</id>"
  doc_line metainfo '<metadata_license>CC0-1.0</metadata_license>'
  doc_line metainfo "<project_license>${license}</project_license>"
  doc_line metainfo "<name>${name}</name>"
  doc_line metainfo "<summary>${summary}</summary>"
  doc_line readme "${name}"
  doc_line readme "$(echo "${name}" | sed s/./=/g)"
  doc_line readme ''
  doc_line readme "${summary}"
}

doc_foot() {
  doc_line html '</div>'
  doc_line html '</body>'
  doc_line html '</html>'
  doc_line metainfo '<categories>'
  doc_line metainfo '<category>Game</category>'
  doc_line metainfo '<category>LogicGame</category>'
  doc_line metainfo '</categories>'
  doc_line metainfo '<launchable type="desktop-id">'"${id}"'.desktop</launchable>'
  doc_line metainfo '<url type="homepage">'"${website}"'</url>'
  doc_line metainfo '<url type="bugtracker">'"$issue_tracker"'</url>'
  doc_line metainfo '<provides>'
  doc_line metainfo "<binary>${binary}</binary>"
  doc_line metainfo '</provides>'
  doc_line metainfo '</component>'
}

doc_p() {
  doc_line html '<p>'
  doc_line man '.PP'
  doc_line readme ''
}

doc_p_line() {
  doc_line html "$1"
  doc_line man "$1"
  doc_line readme "$1"
}

doc_p_link() {
  doc_line html "$1"'<a href="'"$4"'">'"$2</a>$3"
  doc_line man "$1$2$3"
  doc_line readme "$1$2$3"
}

doc_p_end() {
  doc_line html '</p>'
}

doc_linkline() {
  doc_line html '<p>'
  doc_line html '<a href="'"$2"'">'"&#x2192; $1</a>"
  doc_line html '</p>'
  doc_line man ".TP"
  doc_line man "$1"
  doc_line man "$2"
  doc_line readme ''
  doc_line readme "  $1"
  doc_line readme "  <$2>"
}

doc_pre() {
  doc_line html '<pre>'
  doc_line readme ''
}

doc_pre_line() {
  doc_line html "$1"
  doc_line readme "    $1"
}

doc_pre_end() {
  doc_line html '</pre>'
}

doc_dl() {
  doc_line html '<dl>'
}

doc_dl_item() {
  doc_line html "<dt>$1</dt>"
  doc_line html '<dd>'
  doc_line man '.TP'
  doc_line man "$1"
  doc_line readme ''
  doc_line readme "*$1*"
}

doc_dl_item_line() {
  doc_p_line "$1"
}

doc_dl_item_link() {
  doc_p_link "$1" "$2" "$3" "$4"
}

doc_dl_item_end() {
  doc_line html '</dd>'
}

doc_dl_end() {
  doc_line html '</dl>'
}

doc_description() {
  doc_line metainfo '<description>'
  doc_line man '.SH DESCRIPTION'
}

doc_description_p() {
  doc_p
  doc_line metainfo '<p>'
}

doc_description_p_line() {
  doc_p_line "$1"
  doc_line metainfo "$1"
}

doc_description_p_link() {
  doc_p_link "$1" "$2" "$3" "$4"
  doc_line metainfo "$1$2$3"
}

doc_description_p_end() {
  doc_p_end
  doc_line metainfo '</p>'
}

doc_description_end() {
  doc_line metainfo '</description>'
}

doc_nav() {
  doc_line html '<nav>'
}

doc_nav_line() {
  doc_line html '<a href="#'"$2"'">'"$1"'</a>'
}

doc_nav_end() {
  doc_line html '</nav>'
}

doc_man_section() {
  doc_line man ".SH $1"
}

doc_section() {
  doc_line html '<h2 id="'"$2"'">'"$1</h2>"
  doc_line readme ''
  doc_line readme "$1"
  doc_line readme "$(echo "$1" | sed s/./=/g)"
}

doc_subsection() {
  doc_line html '<h3 id="'"$2"'">'"$1</h3>"
  doc_line man ".SS $1"
  doc_line readme ''
  doc_line readme "$1"
  doc_line readme "$(echo "$1" | sed s/./-/g)"
}

doc_release() {
  if [ "$(cat src/version)" = "${1}~dev" ]; then
    doc_subsection "Upcoming Release $1" "$1"
  else
    doc_subsection "Release $1" "$1"
  fi
}

doc_author() {
  if [ "$3" = "" ]; then
    doc_line authors "$(printf "%-20s%s  %-25s  %s" "$1" "$2" "" "$4")"
  else
    doc_line authors "$(printf "%-20s%s  %-25s  %s" "$1" "$2" "<$3>" "$4")"
  fi
  doc_dl_item "$1:"
  doc_dl_item_line "$4"
}

doc
